<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Service\IdentifierChecker;
use Doctrine\ORM\EntityManagerInterface;
use ParagonIE\Halite\KeyFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;
use ParagonIE\Halite\Symmetric\Crypto as SymmetricCrypto;
use ParagonIE\HiddenString\HiddenString;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\HttpKernel\KernelInterface;

class SecurityController extends AbstractController
{

    public function __construct(
        private IdentifierChecker $identifierChecker,
        private TranslatorInterface $translator,
        private UserPasswordHasherInterface $hash,
        private KernelInterface $kernel,
        private $keyFilePath,
        private UserPasswordHasherInterface $passwordHasher,
        private EntityManagerInterface $em
    )
    {
        $this->keyFilePath = $keyFilePath;
    }

    #[Route(path: '/login', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('app_homepage');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    #[Route(path: '/logout', name: 'app_logout')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    #[Route(path: '/forgot-pass', name: 'app_forgot_pass')]
    public function forgotPass(Request $request, UserRepository $userRepository): Response
    {
        $form = $this->createFormBuilder()
            ->add('email', EmailType::class, [
                'label' => 'bo.user.email',
                'required' => true
            ])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // Récupérer les données du formulaire
            $data = $form->getData();
            $isIdentifierExist = $this->identifierChecker->identifierExist($data['email']);
            if(!$isIdentifierExist){
                $this->addFlash('error', $this->translator->trans('app.identifier.not.exist'));
                return $this->redirectToRoute('app_forgot_pass');
            }
            $user = $userRepository->findOneBy(['email' => $data['email']]);
            $key = KeyFactory::generateEncryptionKey();
            KeyFactory::save($key, $this->keyFilePath);
            $userId = strval($user->getId());
            $encryptId = SymmetricCrypto::encrypt(new HiddenString($userId), $key);
            
            return $this->redirectToRoute('app_change_pass', ['encryptId' => $encryptId]);
        }

        return $this->render('security/forgot_pass.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route(path: '/change-pass/{encryptId}', name: 'app_change_pass')]
    public function changePass(string $encryptId, Request $request, IdentifierChecker $identifierChecker, UserRepository $userRepository): Response
    {
        $key = KeyFactory::loadEncryptionKey($this->keyFilePath);
        $decryptId = SymmetricCrypto::decrypt($encryptId, $key); 
        $user = $userRepository->find(intval($decryptId->getString()));
        $form = $this->createFormBuilder()
            ->add('password', RepeatedType::class, [
                'required' => true,
                'type' => PasswordType::class,
                'first_options' => [
                    'label' => 'bo.user.password',
                    'attr' => [
                        'autocomplete' => 'new-password'
                    ]
                ],
                'second_options' => [
                    'label' => 'bo.user.password.confirm',
                    'attr' => [
                        'autocomplete' => 'new-password'
                    ]
                ],
            ])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // Récupérer les données du formulaire
            $data = $form->getData();
            $encodedPassword = $this->passwordHasher->hashPassword(
                $user,
                $data['password']
            );
            $user->setPassword($encodedPassword);
            $this->em->flush();

            $this->addFlash('success', 'Votre mot de passe a été modifier avec succès.');
            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/change_pass.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
