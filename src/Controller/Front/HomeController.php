<?php

namespace App\Controller\Front;

use App\Entity\Application;
use App\Entity\Offer;
use App\Form\ApplicationType;
use App\Repository\ApplicationRepository;
use App\Repository\OfferRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class HomeController extends AbstractController
{
    public function __construct(
        private TranslatorInterface $traslator, 
        private OfferRepository $offerRepository,
        private ApplicationRepository $applicationRepository,
        private EntityManagerInterface $em
    )
    {
        
    }

    #[Route('/', name: 'home_page', methods: ['GET'])]
    public function index() : Response 
    {
        $offers = $this->offerRepository->findAll();
        return $this->render('front/home/index.html.twig', [
            'offers' => $offers,
            'homeActive' => true
        ]);
    }

    #[Route('/apply/{offerId}', name: 'apply_offer')]
    public function apply(int $offerId, Request $request): Response
    {
        $offer = $this->offerRepository->find($offerId);
        $application = new Application();
        $form = $this->createForm(ApplicationType::class, $application);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $application->setOffer($offer);
            $application->setCreatedAt(new \DateTimeImmutable("now"));

            $this->em->persist($application);
            $this->em->flush();
            $this->addFlash('success', $this->traslator->trans('fo.confirmation.apply'));
            return $this->redirectToRoute('apply_offer', ['offerId' => $offerId]);
        }

        return $this->render('front/home/apply.html.twig', [
            'form' => $form,
            'homeActive' => true
        ]);
    }
}