<?php

namespace App\Controller\Admin;

use App\Entity\Company;
use App\Form\CompanyType;
use App\Repository\CompanyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/company')]
class CompanyController extends AbstractController
{
    public function __construct(
        private TranslatorInterface $traslator, 
        private CompanyRepository $companyRepository,
        private EntityManagerInterface $em
    )
    {
        
    }

    #[Route('/', name: "admin_company", methods: ['GET'])]
    public function index(): Response
    {
        $companies = $this->companyRepository->findAll();
        return $this->render('admin/company/index.html.twig', [
            'companies' => $companies,
            'companyActive' => true
        ]);
    }

    #[Route('/new', name: 'add_company')]
    public function new(Request $request): Response
    {
        $company = new Company();
        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $company->setCreatedAt(new \DateTimeImmutable("now"));

            $this->em->persist($company);
            $this->em->flush();
            $this->addFlash('success', $this->traslator->trans('bo.confirmation.add'));
            return $this->redirectToRoute('admin_company');
        }

        return $this->render('admin/company/new.html.twig', [
            'form' => $form,
            'companyActive' => true
        ]);
    }

    #[Route('/edit/{id}', name: 'edit_company')]
    public function edit(Company $company, Request $request): Response
    {
        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $company->setUpdatedAt(new \DateTimeImmutable("now"));
            $this->em->flush();
            $this->addFlash('success', $this->traslator->trans('bo.confirmation.edit'));
            return $this->redirectToRoute('admin_company');
        }

        return $this->render('admin/company/edit.html.twig', [
            'form' => $form,
            'companyActive' => true
        ]);
    }

    #[Route('/show/{id}', name: 'show_company')]
    public function show(Company $company)
    {
        return $this->render('admin/company/show.html.twig', [
            'company' => $company,
            'companyActive' => true
        ]);
    }

    #[Route('/delete/{id}', name: "delete_company", methods: ['GET'])]
    public function delete(Company $company, Request $request)
    {
        if($request->isXmlHttpRequest()){
            $this->em->remove($company);
            $this->em->flush();
            return $this->json(['status' => 'success', 'message' => $this->traslator->trans('bo.confirmation.delete')]);
        }
    }

}