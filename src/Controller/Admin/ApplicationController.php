<?php 

namespace App\Controller\Admin;

use App\Entity\Application;
use App\Repository\ApplicationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/application')]
class ApplicationController extends AbstractController
{
    public function __construct(
        private TranslatorInterface $traslator, 
        private ApplicationRepository $applicationRepository,
        private EntityManagerInterface $em
    )
    {
        
    }

    #[Route('/', name: 'admin_application', methods: ['GEt'])]
    public function index() : Response
    {
        if($this->getUser()->getUserType() === 'admin'){
            $applications = $this->applicationRepository->findAll();
        } else {
            $applications = $this->applicationRepository->findApplicationsForUser($this->getUser()->getId());
        }
        return $this->render('admin/application/index.html.twig', [
            'applications' => $applications,
            'applicationActive' => true
        ]);
    }

    #[Route('/show/{id}', name: 'show_application')]
    public function show(Application $application)
    {
        return $this->render('admin/application/show.html.twig', [
            'application' => $application,
            'applicationActive' => true
        ]);
    }

    #[Route('/delete/{id}', name: "delete_application", methods: ['GET'])]
    public function delete(Application $application, Request $request)
    {
        if($request->isXmlHttpRequest()){
            $this->em->remove($application);
            $this->em->flush();
            return $this->json(['status' => 'success', 'message' => $this->traslator->trans('bo.confirmation.delete')]);
        }
    }

}