<?php

namespace App\Controller\Admin;

use App\Entity\Offer;
use App\Form\AdminOfferType;
use App\Form\OfferType;
use App\Repository\OfferRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/offer')]
class OfferController extends AbstractController
{

    const ADMIN_USER = 'admin';

    public function __construct(
        private TranslatorInterface $traslator, 
        private OfferRepository $offerRepository,
        private EntityManagerInterface $em
    )
    {
        
    }

    #[Route('/', name: 'admin_offer', methods: ['GET'])]
    public function index(): Response
    {
        if($this->getUser()->getUserType() === self::ADMIN_USER){
            $offers = $this->offerRepository->findAll();
        } else {
            $offers = $this->offerRepository->findBy(['owner' => $this->getUser()]);
        }
        return $this->render('admin/offer/index.html.twig', [
            'offers' => $offers,
            'offerActive' => true
        ]);
    }

    #[Route('/new', name: 'add_offer')]
    public function new(Request $request): Response
    {
        $offer = new Offer();
        if($this->getUser()->getUserType() === self::ADMIN_USER){
            $form = $this->createForm(AdminOfferType::class, $offer);
        } else {
            $form = $this->createForm(OfferType::class, $offer);
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($this->getUser()->getUserType() !== self::ADMIN_USER){
                $offer->setOwner($this->getUser());
            }
            $offer->setCreatedAt(new \DateTimeImmutable("now"));

            $this->em->persist($offer);
            $this->em->flush();
            $this->addFlash('success', $this->traslator->trans('bo.confirmation.add'));
            return $this->redirectToRoute('admin_offer');
        }

        return $this->render('admin/offer/new.html.twig', [
            'form' => $form,
            'offerActive' => true
        ]);
    }

    #[Route('/show/{id}', name: 'show_offer')]
    public function show(Offer $offer)
    {
        return $this->render('admin/offer/show.html.twig', [
            'offer' => $offer,
            'offerActive' => true
        ]);
    }

    #[Route('/edit/{id}', name: 'edit_offer')]
    public function edit(Offer $offer, Request $request): Response
    {
        if($this->getUser()->getUserType() === self::ADMIN_USER){
            $form = $this->createForm(AdminOfferType::class, $offer);
        } else {
            $form = $this->createForm(OfferType::class, $offer);
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if(!$this->getUser()->getUserType() === self::ADMIN_USER){
                $offer->setOwner($this->getUser());
            }
            $offer->setCreatedAt(new \DateTimeImmutable("now"));

            $this->em->flush();
            $this->addFlash('success', $this->traslator->trans('bo.confirmation.edit'));
            return $this->redirectToRoute('admin_offer');
        }

        return $this->render('admin/offer/edit.html.twig', [
            'form' => $form,
            'offerActive' => true
        ]);
    }

    #[Route('/delete/{id}', name: "delete_offer", methods: ['GET'])]
    public function delete(Offer $offer, Request $request)
    {
        if($request->isXmlHttpRequest()){
            $this->em->remove($offer);
            $this->em->flush();
            return $this->json(['status' => 'success', 'message' => $this->traslator->trans('bo.confirmation.delete')]);
        }
    }

}