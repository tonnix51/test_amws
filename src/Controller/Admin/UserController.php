<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\EditUserType;
use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/user')]
class UserController extends AbstractController
{

    public function __construct(
        private TranslatorInterface $traslator, 
        private UserRepository $userRepository,
        private EntityManagerInterface $em
        )
    {
        
    }

    #[Route('/', name: 'admin_user', methods: ['GET'])]
    public function index(): Response 
    {
        $users = $this->userRepository->findAll();
        return $this->render('admin/user/index.html.twig', [
            'users' => $users,
            'userActive' => true
        ]);
    }

    #[Route('/new', name: 'add_user')]
    public function new(Request $request, UserPasswordHasherInterface $userPasswordHasher): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $user->setRoles(["ROLE_ADMIN"]);
            $user->setUserType('utilisateur');
            $user->setCreatedAt(new \DateTimeImmutable("now"));

            $this->em->persist($user);
            $this->em->flush();
            $this->addFlash('success', $this->traslator->trans('bo.confirmation.add'));
            return $this->redirectToRoute('admin_user');
        }

        return $this->render('admin/user/new.html.twig', [
            'form' => $form,
            'userActive' => true
        ]);
    }

    #[Route('/show/{id}', name: "show_user", methods: ['GET'])]
    public function show(User $user)
    {
        return $this->render('admin/user/show.html.twig', [
            'user' => $user,
            'userActive' => true
        ]);
    }

    #[Route('/edit/{id}', name: 'edit_user')]
    public function edit(User $user, Request $request): Response
    {
        $form = $this->createForm(EditUserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setUpdatedAt(new \DateTimeImmutable("now"));
            $this->em->flush();
            $this->addFlash('success', $this->traslator->trans('bo.confirmation.edit'));
            return $this->redirectToRoute('admin_user');
        }

        return $this->render('admin/user/edit.html.twig', [
            'form' => $form,
            'userActive' => true
        ]);
    }

    #[Route('/delete/{id}', name: "delete_user", methods: ['GET'])]
    public function delete(User $user, Request $request)
    {
        if($request->isXmlHttpRequest()){
            $this->em->remove($user);
            $this->em->flush();
            return $this->json(['status' => 'success', 'message' => $this->traslator->trans('bo.confirmation.delete')]);
        }
    }
}