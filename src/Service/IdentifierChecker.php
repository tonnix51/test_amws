<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class IdentifierChecker 
{
    public function __construct(private EntityManagerInterface $em)
    {
        
    }

    public function identifierExist($email)  
    {
        $userRepository = $this->em->getRepository(User::class);
        $user = $userRepository->findOneBy(['email' => $email]);
        return $user !== null;
    }
}