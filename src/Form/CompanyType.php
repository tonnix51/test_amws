<?php

namespace App\Form;

use App\Entity\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;
use Vich\UploaderBundle\Form\Type\VichImageType;

class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('companyName', TextType::class, [
                'label' => 'bo.company.name'
            ])
            ->add('kbis', TextType::class, [
                'label' => 'bo.company.kbis',
                'attr' => [
                    'maxlength' => 100,
                ],
            ])
            ->add('stat', TextType::class, [
                'label' => 'bo.company.stat',
                'attr' => [
                    'maxlength' => 100,
                ],
            ])
            ->add('rcs', TextType::class, [
                'label' => 'bo.company.rcs',
                
            ])
            ->add('phone', TextType::class, [
                'label' => 'bo.company.phone',
                'attr' => [
                    'maxlength' => 20,
                ],
                'constraints' => [
                    new Regex([
                        'pattern' => '/^(?:\+|0)[1-9]\d{1,14}$/',
                        'message' => 'Votre numéro de téléphone doit comporter 20 caractère et doit comporter de 0 ou + devant. '
                    ])
                ],
            ])
            ->add('logoFile', VichImageType::class, [
                'label' => 'bo.company.logo',
                'allow_delete' => true,
                'required' => false,
                'download_uri'  => false,
                'image_uri'  => true,
                'attr' => [
                    'accept' => '.jpg,.jpeg,.png,.bmp'
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
