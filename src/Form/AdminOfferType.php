<?php

namespace App\Form;

use App\Entity\Offer;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminOfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('owner', EntityType::class, [
            'label' => 'bo.offer.owner',
            'class' => User::class,
            'choice_label' => 'username',
            'placeholder' => 'app.select',
        ])
        ->add('title', TextType::class, [
            'label' => 'bo.offer.title'
        ])
        ->add('description', TextareaType::class, [
            'label' => 'bo.offer.description'
        ])
        ->add('salary', NumberType::class, [
            'label' => 'bo.offer.salary',
            'required' => false
        ])
        ->add('location', TextType::class, [
            'label' => 'bo.offer.location'
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Offer::class,
        ]);
    }
}
