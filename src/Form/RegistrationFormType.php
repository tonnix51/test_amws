<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Vich\UploaderBundle\Form\Type\VichImageType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'bo.user.email'
            ])
            ->add('plainPassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'label' => 'bo.user.password',
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Merci de bien remplir votre mot de passe.',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit comporter au moins {{ limit }} caractères',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('lastName', TextType::class, [
                'label' => 'bo.user.last.name',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Merci de remplir votre nom svp.',
                    ]),
                ],
            ])
            ->add('firstName', TextType::class, [
                'label' => 'bo.user.first.name',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Merci de remplir votre prénom svp.',
                    ]),
                ],
            ])

            ->add('address', TextType::class, [
                'label' => 'bo.user.address',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Merci de remplir votre adresse svp.',
                    ]),
                ],
            ])
            ->add('phone', TextType::class, [
                'label' => 'bo.user.phone',
                'attr' => [
                    'maxlength' => 20,
                ],
                'constraints' => [
                    new Regex([
                        'pattern' => '/^(?:\+|0)[1-9]\d{1,14}$/',
                        'message' => 'Votre numéro de téléphone doit comporter 20 caractère et doit comporter de 0 ou + devant.'
                    ])
                ],
            ])
            ->add('company', EntityType::class, [
                'label' => 'bo.company.title',
                'class' => Company::class,
                'choice_label' => 'companyName',
                'placeholder' => 'app.select',
                'required' => false
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'bo.user.image.name',
                'allow_delete' => true,
                'required' => false,
                'download_uri'  => false,
                'image_uri'  => true,
                'imagine_pattern' => 'user_profile_128x128',
                'attr' => [
                    'accept' => '.jpg,.jpeg,.png,.bmp'
                ],
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'You should agree to our terms.',
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
