<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Vich\UploaderBundle\Form\Type\VichImageType;

class EditUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('email', EmailType::class, [
            'label' => 'bo.user.email'
        ])

        ->add('lastName', TextType::class, [
            'label' => 'bo.user.last.name',
            'constraints' => [
                new NotBlank([
                    'message' => 'Merci de remplir votre nom svp.',
                ]),
            ],
        ])
        ->add('firstName', TextType::class, [
            'label' => 'bo.user.first.name',
            'constraints' => [
                new NotBlank([
                    'message' => 'Merci de remplir votre prénom svp.',
                ]),
            ],
        ])

        ->add('address', TextType::class, [
            'label' => 'bo.user.address',
            'constraints' => [
                new NotBlank([
                    'message' => 'Merci de remplir votre adresse svp.',
                ]),
            ],
        ])
        ->add('phone', TextType::class, [
            'label' => 'bo.user.phone',
            'attr' => [
                'maxlength' => 20,
            ],
            'constraints' => [
                new NotBlank([
                    'message' => 'Merci de remplir votre prénom svp.',
                ]),
                new Regex([
                    'pattern' => '/^(?:\+|0)[1-9]\d{1,14}$/',
                    'message' => 'Votre numéro de téléphone doit comporter 20 caractère. '
                ])
            ],
        ])
        ->add('company', EntityType::class, [
            'label' => 'bo.company.title',
            'class' => Company::class,
            'choice_label' => 'companyName',
            'placeholder' => 'app.select',
            'required' => false
        ])
        ->add('imageFile', VichImageType::class, [
            'label' => 'bo.user.image.name',
            'allow_delete' => true,
            'required' => false,
            'download_uri'  => false,
            'image_uri'  => true,
            'attr' => [
                'accept' => '.jpg,.jpeg,.png,.bmp'
            ],
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
