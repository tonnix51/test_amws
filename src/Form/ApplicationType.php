<?php

namespace App\Form;

use App\Entity\Application;
use App\Entity\Offer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ApplicationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('lastName', TextType::class, [
                'label' => 'bo.user.last.name'
            ])
            ->add('firstName', TextType::class, [
                'label' => 'bo.user.first.name'
            ])
            ->add('email', EmailType::class, [
                'label' => 'bo.user.email'
            ])
            ->add('phone', TextType::class, [
                'label' => 'bo.company.phone',
                'attr' => [
                    'maxlength' => 20,
                ],
                'constraints' => [
                    new Regex([
                        'pattern' => '/^(?:\+|0)[1-9]\d{1,14}$/',
                        'message' => 'Votre numéro de téléphone doit comporter 20 caractère et doit comporter de 0 ou + devant. '
                    ])
                ],
            ])
            ->add('resumeFile', FileType::class, [
                'label' => 'bo.application.resume',
                'attr' => [
                    'accept' => '.pdf,.doc,.docx'
                ],
            ])
            ->add('otherFile', FileType::class, [
                'label' => 'bo.application.other',
                'required' => false,
                'attr' => [
                    'accept' => '.pdf,.doc,.docx'
                ],
            ])
            ->add('coverLetter', TextareaType::class, [
                'label' => 'bo.application.cover.letter',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Application::class,
        ]);
    }
}
