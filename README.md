# Installation du Projet

Ce projet nécessite les dépendances suivantes :

- Node.js
- Yarn
- PHP 8.2 ou supérieur
- PostgreSQL

## Installation des Dépendances

1. Cloner le projet depuis le référentiel Git.

2. Installer les dépendances PHP en exécutant la commande suivante :

```
composer install
```
## Installer les dépendances JavaScript via Yarn :

```
yarn install
```

## Configuration de la Base de Données
- Paramétrer la connexion à la base de données PostgreSQL dans le fichier .env.

- Créer la base de données en exécutant la commande suivante :

```
php bin/console doctrine:database:create
```
- Appliquer les migrations pour créer les entités dans la base de données :

```
php bin/console doctrine:migrations:migrate
```

## Création d'un Administrateur
Pour créer un administrateur, exécutez la commande suivante en remplaçant email@admin.com et mdp_admin par l'adresse e-mail et le mot de passe souhaités pour l'administrateur :
```
php bin/console app:add-user email@admin.com mdp_admin --admin
```

## Lancement du Serveur de Développement
Enfin, pour lancer le serveur de développement, exécutez la commande suivante pour compiler les assets :
```
yarn watch
```
et démarrer le serveur local:
```
symfony server:start
```