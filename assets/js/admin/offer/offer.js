$(window).on('load', function() {
    const deleteButtons = document.querySelectorAll('.offer-deleted');
    deleteButtons.forEach(function(element) {
        element.addEventListener('click', function(event) {
            event.preventDefault();
            let userId = element.getAttribute('data-id');
            const confirmMessage = element.getAttribute('data-confirm');
            if(confirm(confirmMessage)){
                $.ajax({
                    url: '/admin/offer/delete/' + userId, // Remplacez cela par le chemin réel de votre action Symfony pour obtenir les commandes
                    type: 'GET',
                    success: function (data) {
                        if(data.status === 'success'){
                            $(".deleted-offer").show().html(data.message);
                            setTimeout(function() {
                                window.location.reload();
                            }, 3000);
                        }
                    }
                });
            }
            
            
        });
    });
});