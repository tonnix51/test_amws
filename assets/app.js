/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

import $ from 'jquery';
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-icons/font/bootstrap-icons.css';
import '../assets/theme/css/lineicons.css'
import '../assets/theme/css/materialdesignicons.min.css'
import '../assets/theme/css/main.css'

/* theme js */
import 'bootstrap/dist/js/bootstrap.bundle.js'
import '../assets/theme/js/jvectormap.min.js'
import '../assets/theme/js/world-merc.js'
import '../assets/theme/js/polyfill.js'
import '../assets/theme/js/main.js'

$(window).on('load', function() {
    // Remove alert on after 5 seconds
    setTimeout(function() {
        $('.alert-dismissible').hide();
    }, 5000);
});
