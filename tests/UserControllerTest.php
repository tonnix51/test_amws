<?php

namespace App\Tests;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{


    public function testIndex()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'njaka.randrianarison@gmail.com',
            'PHP_AUTH_PW' => 'romazava05',
        ]);
    
        $client->request('GET', '/admin/user/');
        $client->followRedirect();
        $this->assertResponseIsSuccessful();
    }

    public function testNew()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'njaka.randrianarison@gmail.com',
            'PHP_AUTH_PW' => 'romazava05',
        ]);

        $client->request('GET', '/admin/user/new');
        $client->followRedirect();
        $this->assertResponseIsSuccessful();
        $client->submitForm('Submit', [
            'registration_form[email]' => 'test@example.com',
            'registration_form[password]' => 'password123',
            'registration_form[lastName]]' => 'test',
            'registration_form[firstName]' => 'test',
            'registration_form[address]' => 'addressTest',
        ]);

        $this->assertResponseRedirects('/admin/user');
    }

    public function testShow()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'njaka.randrianarison@gmail.com',
            'PHP_AUTH_PW' => 'romazava05',
        ]);

        // Retrieve a user id for testing
        $user = $this->getUserFromDatabase($client); // Implement this method to fetch a user from the database
        $client->request('GET', '/admin/user/show/' . $user->getId());
        $client->followRedirect();
        $this->assertResponseIsSuccessful();
    }

    public function testEdit()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'njaka.randrianarison@gmail.com',
            'PHP_AUTH_PW' => 'romazava05',
        ]);

        // Retrieve a user id for testing
        $user = $this->getUserFromDatabase($client); // Implement this method to fetch a user from the database
        $client->request('GET', '/admin/user/edit/' . $user->getId());
        $client->followRedirect();
        $this->assertResponseIsSuccessful();

        // Test the form submission with valid data
        $client->submitForm('Submit', [
            // Fill in form fields here with valid data
        ]);

        $this->assertResponseRedirects('/admin/user');
    }

    public function testDelete()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'njaka.randrianarison@gmail.com',
            'PHP_AUTH_PW' => 'romazava05',
        ]);

        // Retrieve a user id for testing
        $user = $this->getUserFromDatabase($client); // Implement this method to fetch a user from the database
        $client->request('GET', '/admin/user/delete/' . $user->getId());
        $client->followRedirect();
        $this->assertResponseIsSuccessful();
    }

    private function getUserFromDatabase($client)
    {
        $container = $client->getContainer();
        $entityManager = $container->get('doctrine')->getManager();
        
        // Récupérer le repository
        $userRepository = $entityManager->getRepository(User::class);
        return $userRepository->find(1);
    }
}
